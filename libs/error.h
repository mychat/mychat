#ifndef _ERROR_H_
#define _ERROR_H_

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <errno.h>

#include <sys/types.h>
#include <syslog.h>

#include "xcolor.h"

#ifndef FALSE
#define FALSE   0
#endif

#ifndef TRUE
#define TRUE    1
#endif

#define MAXLINE     4096


typedef enum _MsgFlag MsgFlag;


enum _MsgFlag {

    POSITIVE_FLAG= 1,
    NEGATIVE_FLAG,

};

/*void err_quit (const char * msg);
void err_msg  (const char * msg);*/
void usage (char * str);
void err_doit (int errnoflag, int level, const char * fmt, va_list ap);
void err_msg (const char * fmt, ...);
void err_quit (const char * fmt, ...);

void message (MsgFlag flag, const char * msg);

void str_format (char * buffer);

void * ec_calloc (size_t times, size_t size);
void in_dump (int fd);


void
usage (char * str) {
    printf ("Usage: %s <server ip addr>\n", str);
    exit (1);
}

void
err_doit (int errnoflag, int level, const char * fmt, va_list ap) {

    int errno_save, n;
    char buf[MAXLINE + 1];

    errno_save = errno;

    vsnprintf (buf, MAXLINE, fmt, ap);

    n = strlen (buf);
    if (errnoflag) {
        snprintf (buf + n, MAXLINE - n, ": %s", strerror(errno_save));
        strcat (buf, "\n");

        syslog (level, buf);

        fflush(stdout);

        fputs (buf,stderr);
        fflush (stderr);
    }

}


void
err_msg (const char * fmt, ...) {
    
    va_list ap;

    va_start (ap, fmt);
    err_doit (0, LOG_INFO, fmt, ap);
    va_end (ap);

}

void
err_quit (const char * fmt, ...) {

    va_list ap;

    va_start (ap, fmt);
    err_doit (1, LOG_INFO, fmt, ap);
    va_end (ap);

    exit(1);
}
/*void
err_quit (const char * msg)
{
    char err_msg[100];

    sprintf (err_msg, "[%s-%s]%s ERROR: %s", F_RED, F_NULL, F_RED, F_NULL);
    
    strncat (err_msg, msg, (sizeof(err_msg) - strlen(err_msg)));
    
    perror (err_msg);
    exit(-1);

}


void
err_msg (const char * msg)
{

    char wrn_msg[100];
 
    sprintf (wrn_msg, "[%s-%s]%s ERROR: %s", F_YELL, F_NULL, F_YELL, F_NULL);
    
    strncat (wrn_msg, msg, (sizeof(wrn_msg) - strlen(wrn_msg)));
    strncat (wrn_msg, "\n\n", (sizeof(wrn_msg) - strlen(wrn_msg)));

    perror (wrn_msg);

}*/


void
message (MsgFlag flag, const char * msg)
{
    char color[32];
    char signal;
    if (flag == POSITIVE_FLAG) {

        strcpy (color, F_BLUE);
        signal = '+';

    } else if (flag == NEGATIVE_FLAG) {

        strcpy (color, F_RED);
        signal = '-';

    } else {

        return;
    }
    
    printf ("[%s%c%s] %s\n", color, signal, F_NULL, msg );

}


void
str_format (char * buffer)
{
    buffer[(strlen(buffer) - 1)] = 0x00;
}

void *
ec_calloc (size_t times, size_t size)
{
    void * ptr;

    ptr = calloc (times, size);
    if (ptr == NULL) {

        err_msg ("in calloc()");
        return NULL;

    }

    return ptr;
}

void 
in_dump (int fd) 
{

    char ch;
    while (read (fd, &ch, sizeof(ch)) != 0);

}

#endif /* ERROR_H_ */
