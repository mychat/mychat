#ifndef _SERVER_H_
#define _SERVER_H_

#include "xsocket.h"
#include "linkedlist.h"

#define BUFFLEN     2048

/*
 * MACROS
 */
/*#define SA_CAST(o)      ((SA *)(o))
#define SA_CAST_ADDR(o)   ((SA_IN *)&(o))*/


typedef struct _pserver pServer;
struct _pserver {
    
    int sockfd;
    int maxfd;

    SA_IN lhost;

    fd_set rset;
    char buffer[BUFFLEN];
   
};

/* 
 * Prototypes ::
 */

void server_init (void);
void mplex_init (linkedlist * xl);
void listen_mplex (linkedlist * xl);
void cli_mplex (linkedlist * xl);
void mplex_handler (linkedlist * xl);
void warn_everyone (linkedlist * xl, MsgFlag flag, const char * fmt, ...);

void
listen_mplex(linkedlist * xl) {
    
    pServer * serv;
    pUser * user;

    BOOL status;

    //int asw;

    serv = LINKEDLIST_GET_SERV_HANDLER(pServer, xl);
    socklen_t sin_size;
    

    sin_size = sizeof(SA_IN);

    /*
     * if theres some incomming data from any remote host, then
     * server must handle it accepting connection, allocating 
     * a new user to the list e informing that through stdout ::
     */
    if (FD_ISSET(serv->sockfd, &(serv)->rset)) {
        user = linkedlist_user_new ();
        user->sockfd = accept (serv->sockfd, SA_CAST(&(user)->uhost), &sin_size);
        if (user->sockfd > 0) {
            user->have_nick = FALSE;
    
            linkedlist_insert (xl, user);
            status = TRUE;
            //write (user->sockfd, &status, sizeof(BOOL));

            printf ("[SERVER] WARNING: got connection from %s ip through port %d [!!]\n",
                    inet_ntoa (user->uhost.sin_addr), ntohs (user->uhost.sin_port));
        }
        
    }

}


void
cli_mplex (linkedlist * xl) {

    int aux;
    int asw;

    pServer * serv;
    pUser * iterator;
    pUser * it_aux;
    char buffer[BUFFLEN];
    char nick[NICKLEN];

    if (linkedlist_is_empty (xl) == 1) {
        return;
    }

    serv = LINKEDLIST_GET_SERV_HANDLER(pServer, xl);
    memset (&(serv->buffer), 0x00, BUFFLEN);
    for (iterator = xl->begin; iterator; iterator = iterator->next) {

        if(FD_ISSET (iterator->sockfd, &(serv)->rset)) {

            /*
             * if read() fails to receive data from user, then,
             * maybe the user connection was closed by remote host ::
             *
             * However, if read() fails returning zero, it indicates that,
             * the user sent data to us, but that data is just a FIN package
             * which is an indicative of connection close ::
             */
            if ((asw = read (iterator->sockfd, serv->buffer, BUFFLEN)) == 0) {

                aux = iterator->sockfd;
                strcpy (nick, iterator->nick);
                linkedlist_remove (xl, iterator);
                FD_CLR (aux, &(serv)->rset);
                close (iterator->sockfd);

                /*printf ("[%sSERVER%s] USER: %s%s%s disconnected [%s!!%s]\n", 
                        F_YELL, F_NULL, F_YELL, nick, F_NULL, F_YELL, F_NULL);*/
                warn_everyone (xl, POSITIVE_FLAG, "[%sSERVER%s]: %s%s%s disconnected [%s!!%s]\n", 
                        F_YELL, F_NULL, F_YELL, nick, F_NULL, F_YELL, F_NULL);
            
            } else if (asw < 0){

                /*
                 * But and if the returning data from the user through
                 * read() function is less the zero?
                 * Yeah, thats a indicative of a internal error and socket must be
                 * closed, error must be handled and rset must be cleaned up ::
                 *
                 * and of course, user data must be remove from the list ::
                 */
                err_msg ("in read()");

                aux = iterator->sockfd;
                strcpy (nick, iterator->nick);
                linkedlist_remove (xl, iterator);
                FD_CLR (aux, &(serv)->rset);
                close (iterator->sockfd);

            } else {

                /*
                 * But if read() does not fail, so it works and then the server
                 * need to handle the received data from the remote user, and
                 * delivery that to everyone except that user his self::
                 */
                if (iterator->have_nick == FALSE) {

                    strncpy (iterator->nick, serv->buffer, strlen(serv->buffer)); 
                    str_format (iterator->nick);

                    iterator->have_nick = TRUE;

                } else {

                    snprintf (buffer, (strlen(serv->buffer) + strlen(iterator->nick) + 5), "[%s]: %s\n", iterator->nick, serv->buffer );
                    strcpy (serv->buffer, buffer);
                    for (it_aux = xl->begin; it_aux; it_aux = it_aux->next) {
                        
                        if (it_aux->sockfd != iterator->sockfd) {
                            

                            write (it_aux->sockfd, serv->buffer, strlen(serv->buffer));
                            memset (&(serv->buffer), BUFFLEN, 0x00);
    
                        }
    
                    }

                }

            }
    
        }

    }

}

void
mplex_init (linkedlist * xl) {

    pServer * serv;
    pUser * iterator;

    serv = LINKEDLIST_GET_SERV_HANDLER(pServer, xl);

    serv->maxfd = linkedlist_get_higher (xl)+1;
    printf ("\n\t[!!]\tDEBUG\t[!!]\t\n\n");
    linkedlist_show (xl);
    printf ("\n\n\t----------------------\t\n\n");
    if (serv->maxfd == 1) {

        serv->maxfd = serv->sockfd + 1;

    }
    printf ("sockfd: %d | maxfd: %d\n", serv->sockfd, serv->maxfd);
    FD_ZERO (&(serv->rset));
    FD_SET(serv->sockfd, &(serv->rset));

    if (linkedlist_is_empty (xl) == 0) {

        for (iterator = xl->begin; iterator; iterator = iterator->next) {

            FD_SET (iterator->sockfd, &(serv->rset));

        }

    }


}

void
server_init (void) {

    linkedlist xl;
    pServer serv;

    linkedlist_init (&xl);

    
    LINKEDLIST_SET_SERV_HANDLER (xl, &serv);

    serv.sockfd = Socket (AF_INET, SOCK_STREAM, 0);
    sock_config (&(serv.lhost), serv.sockfd);
    Bind (serv.sockfd, &(serv.lhost));
    Listen (serv.sockfd);
    
    for (;;) {
        mplex_handler (&xl);
    }

}


void
mplex_handler (linkedlist * xl) {

    pServer * serv;

    serv = LINKEDLIST_GET_SERV_HANDLER (pServer, xl);

    mplex_init (xl);

    if (select (serv->maxfd, &(serv)->rset, NULL, NULL, NULL) < 0) {
        err_quit ("in select()");
    }
    
    listen_mplex (xl);
    cli_mplex (xl);


}

void
warn_everyone (linkedlist * xl, MsgFlag flag, const char * fmt, ...) {

    pServer * serv;
    pUser * iter;

    va_list ap;
    char buff[MAXLINE+1];

    if (linkedlist_is_empty (xl)) {
        return;
    }

    
    va_start (ap, fmt);
    vsnprintf (buff, MAXLINE, fmt, ap);
    va_end (ap);

    if (flag == POSITIVE_FLAG) {
        printf ("%s", buff);
    }

    for (iter = xl->begin; iter; iter = iter->next) {
        write (iter->sockfd, buff, strlen(buff));
    }
    

}
#endif /* _SERVER_H_ */
