#ifndef _XSOCKET_H_
#define _XSOCKET_H_

#include <sys/types.h>
#include <sys/socket.h>
#include <sys/select.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>

#include <unistd.h>

#include "error.h"

#define BACKLOG     1024
#define SERV_PORT   54321


/*
 * Macros ::
 */
#define max(a,b)    ((a) > (b) ? (a) : (b))
#define min(a,b)    ((a) < (b) ? (a) : (b))

#define SA          struct sockaddr
#define SA_IN       struct sockaddr_in

//#define SA_CAST_ADDR(obj)    ((SA *)&(obj))
#define SA_CAST(obj)         ((SA *)(obj))


/*
 * Prototypes ::
 */
int Socket (int family, int type, int protocol);

void sock_config (SA_IN * host_addr, int sockfd);
void Bind (int sockfd, SA_IN * host_addr);
void Listen (int sockfd);
void Connect (int sockfd, SA_IN * addr, socklen_t sin_size);


/*
 * just a simple trial to improve sand and recv
 * functions ::
 */
void Send (int sockfd, char * buffer);
void Recv (int sockfd, char * buffer);


int 
Socket (int family, int type, int protocol)
{   

    int sockfd;
    
    sockfd = socket (family, type, protocol);
    if (sockfd == -1) {

        err_quit ("in socket()");

    }
  
    message (POSITIVE_FLAG, "Socket() is ok ...");
    return sockfd;

}


void
sock_config (SA_IN * host_addr, int sockfd)
{
    int yes = 1;
    host_addr->sin_family = AF_INET;
    host_addr->sin_port = htons (SERV_PORT);
    host_addr->sin_addr.s_addr = INADDR_ANY;
    memset (&(host_addr->sin_zero), 0x00, 8);

    if ( setsockopt (sockfd, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(int)) < 0) {

        err_msg ("in setsockopt ()");

    }

}

void
Bind (int sockfd, SA_IN * host_addr)
{
    if (bind (sockfd, SA_CAST(host_addr), sizeof(SA_IN)) < 0) {

        err_quit ("in Bind ()");

    }

    message (POSITIVE_FLAG, "Bind () is ok ...");
}


void
Listen (int sockfd)
{
    if (listen (sockfd, BACKLOG) < 0) {

        err_quit ("in Listen ()");

    }

    message (POSITIVE_FLAG, "Listen () is ok ...");
}


void 
Send (int sockfd, char * buffer)
{
    size_t length = sizeof (buffer);
    size_t sent;

    sent = send (sockfd, buffer, length, 0);
    if (sent < 0) {
        err_quit ("int send()");
    }else if (sent < length) {
    
        while (sent < length) {

            length = length - sent;
            buffer = buffer+sent;
            sent = send(sockfd, buffer, length, 0);

        }

    }

}

void
Connect (int sockfd, SA_IN * hostaddr, socklen_t sin_size) {
    int con;

    con = connect (sockfd, SA_CAST(hostaddr), sin_size);
    if (con < 0) {
        err_quit ("in connect ()");
    }

    message (POSITIVE_FLAG, "connect () is ok...");
}

#endif /* _XSOCKET_H_ */

