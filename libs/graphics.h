#ifndef _GRAPHICS_H_
#define _GRAPHICS_H_

#include <stdlib.h>
#include <string.h>

#include <ncurses.h>

#ifndef NICKLEN
#define NICKLEN 32
#endif

typedef struct _currentpos CurrentPos;

struct _currentpos {
    int x;
    int y;
};

typedef enum _usertype UserType;
typedef enum _WindowType WindowType;

enum _usertype {

    REMOTE = 0,
    LOCAL,

};

enum _WindowType {

    RECV_WINDOW = 0,
    SEND_WINDOW,

};


int recvx;
char nick[NICKLEN];

CurrentPos current;

WINDOW * new_window (WindowType Type);

void sendwin_refresh (WINDOW * sendwin);
void recvwin_refresh (WINDOW * recvwin);
void printw_recvwindow (WINDOW * recvwin, UserType type, char * msg);

void destroy_window (WINDOW * win);



/*
 * **@brief: type will define what kind
 * of window must be drawn.
 */
WINDOW *
new_window (WindowType type) {

    WINDOW * window = (WINDOW *) calloc (1, sizeof (WINDOW));

    int h, w;
    int startx, starty;

    getmaxyx(stdscr, h, w);

    if (type == RECV_WINDOW) {
        h = (h - recvx) - 2.5;
        w = (w - 1);

        startx = 0;
        starty = (COLS/2)-(w/2);

    } else if (type == SEND_WINDOW) {
        h = 3;
        w = COLS-1;

        startx = (LINES - h);
        starty = (COLS/2)-(w/2);

        recvx = startx;
    }

    window = newwin (h, w, startx, starty);

    box (window, 0, 0);
    scrollok(window, TRUE);

    mvwprintw (window, 1, 1, "");

    if (type == SEND_WINDOW) {
        sendwin_refresh (window);
    } else {
        current.x = 1;
        current.y = 1;
        wrefresh (window);
    }

    return window;

}


void
sendwin_refresh (WINDOW * sendwin) {

    wclear (sendwin);
    mvwprintw (sendwin, 1, 1, "[%s]> ", nick);

    box(sendwin, 0, 0);

    wrefresh (sendwin);

}

void
recvwin_refresh (WINDOW * recvwin) {

    box (recvwin, 0, 0);

    wrefresh (recvwin);
}

void 
printw_recvwindow (WINDOW * recvwin, UserType type, char * msg) {

    if (type == LOCAL) {
        mvwprintw (recvwin, current.x, current.y, "[%s]> %s\n", nick, msg);
    } else {
        mvwprintw (recvwin, current.x, current.y, "%s\n", msg);
    }

    recvwin_refresh (recvwin);
    current.x++;
    current.y;

}




void
destroy_window (WINDOW * win) {
    wborder (win, ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ');
    wclear (win);
    wrefresh (win);

    delwin (win);

    free(win);
}

#endif /* _GRAPHICS_H_ */
