#ifndef _LINKEDLIST_H_
#define _LINKEDLIST_H_

#include "error.h"

// Macros ::
#define LINKEDLIST_GET_SERV_HANDLER(type,ptr)          ((type *)((ptr)->serv_handler))
#define LINKEDLIST_SET_SERV_HANDLER(xlptr,servptr)     (xlptr.serv_handler = servptr)
#define ALLOC(n,type)                                  ((type *)(ec_calloc((n), sizeof(type))))
#define max(a,b)                                       ((a) > (b) ? (a) : (b))
#define LINKEDLIST(o)                                  ((linkedlist *)(o));

#ifndef NICKLEN
#define NICKLEN     32
#endif

#ifndef TRUE
#define TRUE    1
#endif

#ifndef FALSE
#define FALSE   0
#endif

#define BOOL        unsigned char



typedef struct _puser pUser;
typedef struct _linkedlist linkedlist;

typedef void (callback) (void * arg); 

struct _puser {

    char nick [NICKLEN];
    int sockfd;

    BOOL have_nick;

    SA_IN uhost;

    pUser * next;

};


struct _linkedlist {
    
    pUser * begin;
    pUser * end;
    int counter;

    void * serv_handler;

};


/*
 * Prototypes ::
 */

int linkedlist_is_empty (linkedlist * xl);
void linkedlist_do_for_each (linkedlist * xl, callback * callback_func);
void linkedlist_init (linkedlist * xl);
void linkedlist_insert (linkedlist * xl, pUser * user);
void linkedlist_remove (linkedlist * xl, pUser * user);
void linkedlist_show (linkedlist * xl);

void linkedlist_free (linkedlist * xl);

pUser * linkedlist_user_new (void);

int linkedlist_get_higher (linkedlist * xl);



int
linkedlist_is_empty (linkedlist * xl) {
    if (xl->counter == 0) {
        return 1;
    }
    return 0;
}



void
linkedlist_init (linkedlist * xl) {

    xl->begin = NULL;
    xl->end = NULL;
    xl->serv_handler = NULL;

    xl->counter = 0;

}



void
linkedlist_insert (linkedlist * xl, pUser * user) {

    user->next = 0;
    if (linkedlist_is_empty (xl) == 1) {

        xl->begin = user;
        xl->end = user;
        xl->counter++;

    } else {

        xl->end->next = user;
        xl->end = user;
        xl->counter++;

    }

}



void
linkedlist_remove (linkedlist * xl, pUser * user) {

    pUser * iterator;
    pUser * prv;
    if (linkedlist_is_empty (xl) == 1) {
        message (POSITIVE_FLAG, "list is already empty");
        return ;
    }

    
    if (user->sockfd == xl->begin->sockfd) {
        prv = xl->begin;
        xl->begin = xl->begin->next;
        free (prv);
        xl->counter--;
        return;
    }

    for(iterator = xl->begin; iterator; iterator = iterator->next) {

       if (iterator->sockfd == user->sockfd) {
           break;
       }
       prv = iterator;

    }

    prv->next = iterator->next;
    free(iterator);
    xl->counter --;

}



void
linkedlist_show (linkedlist * xl) {

    pUser * iterator;

    if (linkedlist_is_empty (xl) == 1) {
        message (NEGATIVE_FLAG, "list is already empty");
        return;
    }

    for (iterator = xl->begin; iterator; iterator = iterator->next) {

        printf ("FD: %d, NICK: %s\n", iterator->sockfd, iterator->nick);

    }
}


void
linkedlist_free (linkedlist * xl) {

    pUser * iterator;
    pUser * prv;

    iterator = xl->begin;

    while (iterator != NULL) {
        prv = iterator;
        iterator = iterator->next;

        free(prv);
    }
}


pUser *
linkedlist_user_new (void) {

    return ALLOC(1, pUser);

}

int
linkedlist_get_higher (linkedlist * xl) {

    int higher;

    pUser * iterator;

    higher = 0;
    if (linkedlist_is_empty (xl) == 0) {

        for (iterator = xl->begin; iterator; iterator = iterator->next) {

            higher = max(higher, iterator->sockfd);

        }

    }

    return higher;
}


void
linkedlist_do_for_each (linkedlist *xl, callback * callback_func) {
    
    callback_func ( (void *) xl);
    
}

#endif /* _LINKEDLIST_H_ */
