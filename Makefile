CC=gcc
FLAGS=-o
DEST=../server
LIBS=linkedlist.o error.o xsocket.o

server:
	$(CC) server.c  $(LIBS) $(FLAG) $(DEST)

clean:
	make -C libs/ -f Makefile cleanall

